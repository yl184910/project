/**********************************************\
	Justin Archer
	CS 2401 Fall 2013
	Project 6
	Professor Dolan
\**********************************************/



/**********************************************\
	TODO:
	1.) Create a separate "Logger" class.
	In this Logger class, we need to establish
	some sort of correspondence between the roads
	originally read in from "roads.dat" and what
	we store in our "logfile.dat". One way to do
	this would be with sorting after returning
	all of the Roads back to ross_map. Another
	way would be to never actually remove anything
	from ross_maps. A final alternative would be
	to establish the correspondence trivially
	by including all of the Roads in "logfile.dat".

	The code from this logger class should be
	included in every place where we make an action
	worth logging. This will include the scheduler,
	and also some of the Plow class member functions
	(such as assign_job and do_job).

\**********************************************/

#include "plow.h"
#include "road.h"
#include "logger.h"
#include <iostream> // console io
#include <fstream>  // used for file io
#include <cstdlib>  // used for exit(1) and atoi()
					// and srand()
#include <cctype>   // used for is_digit()
#include <string>
#include <queue>
#include <ctime>	// used for time()
#include <cmath>	// used for fmod()
#include <vector>

// These variables/objects can be accessed
// externally.

logger theLogger("logfile.dat");
double hours = 0;
int snow_factor;

using namespace std;

// Function Predeclarations

int rand_gen(int lower, int upper);

bool operator < (const Road& LHS, const Road& RHS);
// Used for "priority_queue<Road>" objects; i.e. "jobs"

// These are command line arguments
int main( int argc, char *argv[])
{
	if (argc != 3){
		cout << "usage: " << argv[0] << endl;
		exit(1);
	}

	srand(time(NULL));
	// Seed (aka initialize) rand()
	int max_hours = atoi(argv[1]);
	snow_factor = atoi(argv[2]);

	vector<Road> ross_map;
	// Since our data file is of a constant size
	// we don't need dynamic memory i.e. lists
	// or dynamic arrays.
	int map_used = 0;
	// How many elements of our array are used.
	priority_queue<Road> jobs;
	Plow fleet[6];

	//Variables for statistics
	double miles_plowed = 0;
	double idle_time = 0;
	double transit_time = 0;
	double road_wait_time = 0;
	int road_wait_num = 0;

	/******************************************\
		
		Read "road.dat" into list<Road> ross_map

	\******************************************/

	ifstream read; // this will be used to read
	// our "road.dat" into ross_map
	read.open("roads.dat");

	if(read.fail())
	{
		cerr << "Failed to open data file... exiting"
		<< endl;
		exit(1);
	}

	while(isalnum(read.peek()))
	{
		string junk;
		Road temp;
		read >> temp.priority;
		getline(read, temp.name, '(' );
		read >> temp.coord.first;
		read.ignore(1, ',');
		read >> temp.coord.second;
		read.ignore(1, ')');
		read >> temp.length;
		getline(read, junk, '\n');
		ross_map.push_back(temp);
	}

	read.close();


	/******************************************\

		Schedule the movements of plows in the
		fleet according to which plow is best
		suited to do each particular job, while
		also adding, with a 10% probablity, a new
		"Road" to "jobs" every minute.

		We begin by establishing a "time_scale",
		which tells us how much forward in time
		we can skip to until some significant 
		action needs to be taken: either assigning
		new jobs or checking to see whether or not
		a new job will be generated.

		After this we do_job(time_scale); on all of
		the Plow objects in our fleet of snowplows.

		Finally, we adjust "hours" (the remaining
		simulation time) and "call_time" (how much
		time is left until we need to check for
		a randomly generated call).

	\******************************************/

	double call_time = 0;
	double time_scale = 0;

	while(max_hours > hours)
	{
		// If any plows have just become idle, here we
		// return their jobs back to ross_map so that they
		// may be once more randomly selected to be placed
		// in the priority queue "jobs".

		for(int i = 0; i < 6; ++i)
		{	
			if(fleet[i].is_idle() && fleet[i].has_job())
			{
				Road temp;
				temp = fleet[i].remove_job();

				ross_map.push_back(temp);
			}
		}

		// Every minute the simulator should check and see whether
		// or not a new call has been placed; calls are placed
		// on average once every ten minutes. If a call has
		// been placed, a new job needs to be extracted from
		// "ross_map" and placed into the priority queue "jobs".

		if( call_time == 0 )
		{	
			if(9 == rand_gen(0,9))
			{
				int index;
				bool available = 0;

				while(available == 0)
				{
				  index = rand_gen(0, ross_map.size()-1);
					if(ross_map[index].priority != 0)
					available = 1;
				}

				jobs.push( ross_map[index] );
				theLogger.set_time(hours);
				theLogger.call_placed(ross_map[index]);
				ross_map.erase(ross_map.begin()+index);
			}
		}

		// Finds jobs for all of the idle "Plow"s.

		while(!jobs.empty())
		{
			int least_dist;
			int least_index = 6;
			
			// Finds the first idle "Plow"

			for(int i = 0; i < 6; ++i)
			{
				if(fleet[i].is_idle())
				{
					least_index = i;
					break;
				}
			}

			// If there are no idle plows we can't assign anymore
			// jobs and so we break the while(!jobs.empty()) loop

			if(least_index > 5) {
			  road_wait_time += time_scale;
			  road_wait_num++;
			  break;
			}
			// Finds the closest available "Plow"
			// for the job currently at the top
			// of "jobs" and assigns the job to it.

			least_dist = fleet[least_index].distance(jobs.top());

			for(int i = least_index + 1; i < 6; ++i)
			{
				if(fleet[i].is_idle()
				&&  least_dist >= fleet[i].distance(jobs.top()))
				{
					least_index = i;
					least_dist  = fleet[i].distance(jobs.top());
				}
			}

			transit_time += fleet[least_index].assign_job(jobs.top());

			//Removes the job from the priority queue "jobs"

			jobs.pop();
		}

		// Finds the time scale for our simulation again,
		// this time though after possibly adding a new job
		// to the priority queue "jobs".

		time_scale = fleet[0].get_time_scale();
		for(int i = 1; i < 6; ++i)
		{
			if(fleet[i].get_time_scale() < time_scale)
				time_scale = fleet[i].get_time_scale();
		}

		// If all of the "Plow"s are still idle, then there
		// is nothing left to do but move forward in time
		// and waiting until a new job is added to the priority
		// queue "jobs"

		if( time_scale == 0.0 || time_scale > 1.0/60.0 - call_time)
		{
			time_scale = (1.0/60.0 - call_time);
		}

		// Updates every "Plow" to the current simulation time

		for( int i = 0; i < 6; ++i)
			fleet[i].do_job(time_scale);

		for(int i = 0; i < 6; ++i) {
		  if(fleet[i].is_idle()) {
		    miles_plowed += fleet[i].get_job_length();
		    idle_time += time_scale;
		  }
		  if(fleet[i].is_in_transit()) {
		    transit_time += time_scale;
		  }
		}

		// Before incrementing the simulation forward in time again,
		// we adjust the simulation time and the time left until
		// we check to see if we need to add more "Road"s from
		// "ross_map" into "jobs".

		hours += time_scale;
		call_time += time_scale;
		if(call_time = 0.0)
			call_time += 1.0/60.0;

		static int x = 0;
		++x;

	} // end of simulation (while) loop

	cout << "Miles of road plowed: " << miles_plowed << endl;
	cout << "Amount of idle time: " << idle_time << endl;
	cout << "Amount of transit time: " << transit_time << endl;
	cout << "Average wait time for plow jobs: " << road_wait_time/road_wait_num << endl;
	cout << "Roads still in queue: " << endl;
	while( !jobs.empty() ) {
	  Road job = jobs.top();
	  cout << "   " << job.priority << " " << job.name << "(" << job.coord.first << "," << job.coord.second << ") " << job.length << endl;
	  jobs.pop();
	}

	return 0;
}



bool operator < (const Road& LHS, const Road& RHS)
{
return LHS.priority < RHS.priority;
}

int rand_gen(int lower, int upper)
{
	return rand()%(upper - lower + 1) + lower;
}
