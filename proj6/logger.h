/**********************************************\
	Levi Triplett
	CS 2401 Fall 2013
	Project 6
	Professor Dolan
\**********************************************/
#ifndef LOGGER_H
#define LOGGER_H
#include <utility>
#include <fstream>
#include <string>
#include "plow.h"
#include "road.h"
//writes to logfile.dat
class logger{
public:

logger(const std::string& fileName);//constructor

logger();//default constructor

void call_placed(const Road& road);

void call_received(const Plow& plow,const Road& road);

void job_finished(const Plow& plow,const Road& road);

void destination_reached(const Plow& plow,const Road& road);

void set_time(double Time);

// void empty_buffer();

private:

std::string filename;

double time;

};
#endif
