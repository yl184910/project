/**********************************************\
	Justin Archer
	CS 2401 Fall 2013
	Project 6
	Professor Dolan
\**********************************************/

#ifndef PLOW_H
#define PLOW_H

#include <utility>
#include "road.h"
//#include "logger.h"


class Plow{
public:

  	Plow();

  	// Acessors

  	double distance(Road some_road ) const;
	// Returns the amount of time it takes
	// for the plow to get to some road.

  	double get_time_scale() const
  	{return _time_left_transit + _time_left_job;}
  	// Returns the sum amount of time needed
  	// to both arrive at _current_job and also
  	// complete _current_job.

	double get_number() const { return _number; }

	double get_job_length() const { return _current_job.length; }

	bool is_idle() const
	{ return _time_left_job == 0;}

	bool is_in_transit() const
	{ return _time_left_transit > 0;}

	bool has_job()
	{_current_job.priority != 0;}

	Road remove_job();

	// Mutators

	//Returns amount of time for transit
	double assign_job(Road job);

	void do_job(double time);

private:
	Road _current_job;
	double _time_left_job;
	double _time_left_transit;
	int _number;
	bool _reached_job;
	bool _finished_job;

	static int TOTAL_PLOWS;
};

#endif
