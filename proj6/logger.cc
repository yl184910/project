/**********************************************\
	Levi Triplett
	CS 2401 Fall 2013
	Project 6
	Professor Dolan
\**********************************************/

#include "logger.h"
#include <list>

using namespace std;
logger::logger(const string& fileName)
{
	filename=fileName;
}

logger::logger()
{
filename="logfile.dat";
}

void logger::set_time(double Time)
{
time=Time;
}

void logger::call_placed(const Road& road)
{
	ofstream file;
	file.open(filename.c_str(), ios::app);
	if(file.is_open())
	{
		file<<time<<": Received a job for road : "<<road.name<<endl; 
	}
	file.close();
}

void logger::call_received(const Plow& plow,const Road& road)
{
   ofstream file;
	file.open(filename.c_str(), ios::app);
	if(file.is_open())
	{
     file<<time<< ": Plow: "<<plow.get_number()<<" assigned to: "<<road.name<<endl;
	}
	file.close();
}

void logger::job_finished(const Plow& plow,const Road& road)
{
   ofstream file;
	file.open(filename.c_str(), ios::app);
	if(file.is_open())
	{
     file<<time<<": Plow: "<<plow.get_number()<<" finished: "<<road.name<<endl;
	}
	file.close();
}

void logger::destination_reached(const Plow& plow,const Road& road)
{
   ofstream file;
	file.open(filename.c_str(), ios::app);
	if(file.is_open())
	{
     file<<time<<": Plow: "<<plow.get_number()<<" reached: "<<road.name<<endl;
	}
	file.close();
}