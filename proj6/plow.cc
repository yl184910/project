/**********************************************\
	Justin Archer
	CS 2401 Fall 2013
	Project 6
	Professor Dolan
\**********************************************/

#include <cmath>
#include <iostream>
#include "plow.h"
#include "logger.h"

extern logger theLogger; //defined in main.cc

extern double hours; //defined in main.cc

extern int snow_factor; //defined in main.cc

int Plow::TOTAL_PLOWS = 0;

Plow::Plow() {
  snow_factor = 1;
  _time_left_transit = 0.0;
  _time_left_job = 0.0;
  _number = ++TOTAL_PLOWS;
  _current_job.coord.first = 0.0;
  _current_job.coord.second = 0.0;
}

double Plow::distance(Road some_road ) const
{
  return sqrt(pow(some_road.coord.first - _current_job.coord.first, 2)
          + pow(some_road.coord.second - _current_job.coord.second, 2));
}

void Plow::do_job(double time)
{

  double _temp_transit_time = _time_left_transit;
  double _temp_job_time = _time_left_job;

  if( _time_left_transit + _time_left_job <= time )
  {
    if(_reached_job == 0)
    {
      theLogger.set_time(hours + _time_left_transit);
      theLogger.destination_reached( *this, _current_job);
      _time_left_transit = 0.0;
      _reached_job = 1;
    }

    if(_finished_job == 0)
    {
    theLogger.set_time(hours + _temp_transit_time + _time_left_job);
    theLogger.job_finished( *this, _current_job);
    _time_left_job = 0.0;
    _finished_job = 1;
    }
  }

  else if(time < _time_left_job && _reached_job == 1)
  {
    _time_left_job -= time;
  }

  else if(_time_left_transit <= time
      && _reached_job == 0 )
  {
    theLogger.set_time(hours + _time_left_transit);
    theLogger.destination_reached( *this, _current_job);
    time -= _time_left_transit;
    _time_left_job -= time;
    _time_left_transit = 0.0;
    _reached_job = 1;
  }

  else if(time < _time_left_transit)
  {
    _time_left_transit -= time;
  }

}

double Plow::assign_job(Road job)
{
  _time_left_transit = distance(job)/35.0;
  _time_left_job = (job.length*snow_factor)/6.0;
  _current_job = job;
  theLogger.set_time(hours);
  theLogger.call_received(*this, job);
  _reached_job = 0;
  _finished_job = 0;

  return _time_left_transit;
}

Road Plow::remove_job()
{
  Road temp;
  temp = _current_job;
  _current_job.priority = 0.0;
  return temp;
}
