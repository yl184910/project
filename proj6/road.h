/**********************************************\
	Justin Archer
	CS 2401 Fall 2013
	Project 6
	Professor Dolan
\**********************************************/

	#ifndef ROAD_H
	#define ROAD_H

	#include <string>
	#include <utility>

	struct Road{

		std::pair<double,double> coord;
		// xy coordinates of some street

		std::string name; // name of street

		int priority;

		double length;

	} ;

	#endif